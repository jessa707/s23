let trainer = {
	name: "Ash ketchum",
	age: 10,
	pokemon: ['Pikachus', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){ 
		console.log(trainer.pokemon[0] + " I choose you!");
		
	}
}


console.log(trainer);

console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of Square bracket notation:')
console.log(trainer['pokemon']);
console.log('Result of Square talk method:');
trainer.talk();



function Pokemon(name, level, health, attack){
	
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//sa loob ng object method
	this.tackle = function(target){
		let targetHealth = target.health - this.attack
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));

		if(targetHealth <= 0){
			target.faint()
		}
	}

	this.faint = function(){
		console.log(this.name + " fainted.")

	}
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12);
console.log(pikachu)
let geodude = new Pokemon("Geodude", 8, 16, 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);


